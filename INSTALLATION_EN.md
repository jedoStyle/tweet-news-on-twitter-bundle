# Installation of Contao NewsTweets Bundle

There are two types of installation.

* with the Contao-Manager, only for Contao Managed-Editon
* via the command line, for Contao Standard-Edition and Managed-Editon


## Installation with Contao-Manager

* search for package: `jedostyle/contao-newstweets-bundle`
* install the package
* Click on "Install Tool"
* Login and update the database


## Installation via command line

### Installation for Contao Managed-Edition

Installation in a Composer-based Contao 4.6+ Managed-Edition:

* `composer require "jedostyle/contao-newstweets-bundle"`
* Call http://yourdomain/contao/install
* Login and update the database


### Installation for Contao Standard-Edition

Installation in a Composer-based Contao 4.3+ Standard-Edition

* `composer require "jedostyle/contao-newstweets-bundle"`

Add in `app/AppKernel.php` following line at the end of the `$bundles` array.

`new JedoStyle\ContaoNewstweetsBundle\ContaoNewstweetsBundle(),`

Clears the cache and warms up an empty cache:

* `bin/console cache:clear --env=prod --no-warmup`
* `bin/console cache:warmup --env=prod`
* Call http://yourdomain/contao/install
* Login and update the database

