<?php

/*
 * This file is part of NewsTweetsBundle.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * This project is provided in good faith and hope to be usable by anyone.
 *
 * @package     Contao4 NewsTweetsBundle
 * @author      Jens Doberenz <WurzelGnOOm>
 * @copyright   jedoLabs 2019 <https://jedo-labs.de>
 * @license     LGPL-3.0-or-later
 * @see	        <https://gitlab.com/jedoLabs/>
 */

namespace JedoLabs\ContaoNewstweetsBundle\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\NewsBundle\ContaoNewsBundle;
use JedoLabs\ContaoNewstweetsBundle\ContaoNewstweetsBundle;
use JedoLabs\ContaoExtensionHelperBundle\ContaoExtensionHelperBundle;


class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(ContaoNewstweetsBundle::class)
                ->setLoadAfter([
                    ContaoCoreBundle::class,
                    ContaoNewsBundle::class,
                    ContaoExtensionHelperBundle::class,
                ]),
        ];
    }
}
