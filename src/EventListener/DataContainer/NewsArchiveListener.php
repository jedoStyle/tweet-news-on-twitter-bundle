<?php

/*
 * This file is part of NewsTweetsBundle.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * This project is provided in good faith and hope to be usable by anyone.
 *
 * @package     Contao4 NewsTweetsBundle
 * @author      Jens Doberenz <WurzelGnOOm>
 * @copyright   jedoLabs 2019 <https://jedo-labs.de>
 * @license     LGPL-3.0-or-later
 * @see	        <https://gitlab.com/jedoLabs/>
 */


namespace JedoLabs\ContaoNewstweetsBundle\EventListener\DataContainer;

use Contao\Controller;
use Contao\System;

use Contao\NewsArchiveModel;
use JedoLabs\ContaoNewstweetsBundle\Model\NewsTwitterAccountsModel;

/**
 * Class NewsArchiveListener.
 */
class NewsArchiveListener
{
    public function toggleTwitterArchive($row, $href, $label, $title, $icon, $attributes)
    {


        if (strlen(\Contao\Input::get('aid'))) {
            $this->toggleTwitterArchiveVisibility(\Contao\Input::get('aid'), ('1' === \Contao\Input::get('state')), (@func_get_arg(12) ?: null));
            Controller::redirect(System::getReferer());
        }

        // Check permissions AFTER checking the tid, so hacking attempts are logged
        if (!\BackendUser::getInstance()->hasAccess('NewsArchiveModel::getTable()::SendNewsToTwitter', 'alexf')) {
            return '';
        }

        $href .= '&amp;aid='.$row['id'].'&amp;state='.($row['SendNewsToTwitter'] ? '' : 1);

        if (!$row['SendNewsToTwitter']) {
            $icon = 'bundles/contaonewstweets/images/WhiteOnImage.svg';
        }

        //return '<a href="'.\Backend::addToUrl($href).'" title="'.\StringUtil::specialchars($title).'"'.$attributes.'>'.\Image::getHtml($icon, $label, 'data-state="'.($row['SendNewsToTwitter'] ? 1 : 0).'"').'</a> ';
        return '<a href="'.\Controller::addToUrl($href).'&rt='.\RequestToken::get().'" title="'.\StringUtil::specialchars($title).'"'.$attributes.'>'.\Image::getHtml($icon, $label, 'data-state="'.($row['published'] ? 1 : 0).'"').'</a> ';
    }

    public function toggleTwitterArchiveVisibility($intId, $blnVisible, \DataContainer $dc = null)
    {
        \Input::setGet('id', $intId);
        \Input::setGet('act', 'toggleTweetArchive');

        if ($dc) {
            $dc->id = $intId;
        }

        if (!\BackendUser::getInstance()->hasAccess(NewsArchiveModel::getTable().'::SendNewsToTwitter', 'alexf')) {
            throw new \Contao\CoreBundle\Exception\AccessDeniedException('Not enough permissions to change twitter stats of NewsArchiv ID '.$intId.'.');
        }

        // Set the current record
        if ($dc) {
            $objRow = \Database::getInstance()->prepare('SELECT * FROM '.NewsArchiveModel::getTable().' WHERE id=?')
                ->limit(1)
                ->execute($intId);

            if ($objRow->numRows) {
                $dc->activeRecord = $objRow;
            }
        }

        $objVersions = new \Versions(NewsArchiveModel::getTable(), $intId);
        $objVersions->initialize();

        // Trigger the save_callback
        if (\is_array($GLOBALS['TL_DCA'][NewsArchiveModel::getTable()]['fields']['SendNewsToTwitter']['save_callback'])) {
            foreach ($GLOBALS['TL_DCA'][NewsArchiveModel::getTable()]['fields']['SendNewsToTwitter']['save_callback'] as $callback) {
                if (\is_array($callback)) {
                    \System::importStatic($callback[0]);
                    $blnVisible = \System::importStatic($callback[0])->{$callback[1]}($blnVisible, $dc);
                } elseif (\is_callable($callback)) {
                    $blnVisible = $callback($blnVisible, $dc);
                }
            }
        }

        $time = time();

        // Update the database
        \Database::getInstance()->prepare('UPDATE '.NewsArchiveModel::getTable()." SET tstamp=$time, SendNewsToTwitter='".($blnVisible ? '1' : '')."' WHERE id=?")
            ->execute($intId);

        if ($dc) {
            $dc->activeRecord->tstamp = $time;
            $dc->activeRecord->SendNewsToTwitter = ($blnVisible ? '1' : '');
        }

        // Trigger the onsubmit_callback
        if (\is_array($GLOBALS['TL_DCA'][NewsArchiveModel::getTable()]['config']['onsubmit_callback'])) {
            foreach ($GLOBALS['TL_DCA'][NewsArchiveModel::getTable()]['config']['onsubmit_callback'] as $callback) {
                if (\is_array($callback)) {
                    \System::importStatic($callback[0])->{$callback[1]}($dc);
                } elseif (\is_callable($callback)) {
                    $callback($dc);
                }
            }
        }

        $objVersions->create();
    }

    public function getTwitterAccoutOptions()
    {

        $accouts = \Database::getInstance()->prepare("SELECT id, account, user_id FROM ".NewsTwitterAccountsModel::getTable()." WHERE published = 1")->execute();

        $TwitterAccoutOptions = array();
        while($accouts->next())
        {
            $UserID = \System::getContainer()->get('jedolabs.extensionhelper.security.encryption')->decryptData($accouts->user_id);
            $TwitterAccoutOptions[$accouts->id] = $accouts->account." [ ".$UserID." ]";
        }
        return $TwitterAccoutOptions;

    }
}
