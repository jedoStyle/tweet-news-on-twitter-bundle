<?php

/*
 * This file is part of NewsTweetsBundle.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * This project is provided in good faith and hope to be usable by anyone.
 *
 * @package     Contao4 NewsTweetsBundle
 * @author      Jens Doberenz <WurzelGnOOm>
 * @copyright   jedoLabs 2019 <https://jedo-labs.de>
 * @license     LGPL-3.0-or-later
 * @see	        <https://gitlab.com/jedoLabs/>
 */


namespace JedoLabs\ContaoNewstweetsBundle\EventListener\DataContainer;

use Contao\Controller;
use Contao\System;
use Contao\NewsModel;
use JedoLabs\ContaoNewstweetsBundle\Library\NewsTweets;

/**
 * Class NewsListener.
 */
class NewsListener
{
    public function toggleTwitterNews($row, $href, $label, $title, $icon, $attributes)
    {


        if (strlen(\Contao\Input::get('nid'))) {
            $this->toggleTwitterNewsVisibility(\Contao\Input::get('nid'), ('1' === \Contao\Input::get('state')), (@func_get_arg(12) ?: null));
            Controller::redirect(System::getReferer());
        }


        // Check permissions AFTER checking the tid, so hacking attempts are logged
        if (!\BackendUser::getInstance()->hasAccess('NewsModel::getTable()::TweetOnTwitter', 'alexf')) {
            return '';
        }

        $href .= '&amp;nid='.$row['id'].'&amp;state='.($row['TweetOnTwitter'] ? '' : 1);

        if (!$row['TweetOnTwitter']) {
            $icon = 'bundles/contaonewstweets/images/WhiteOnImage.svg';
        }

        //return '<a href="'.\Backend::addToUrl($href).'" title="'.\StringUtil::specialchars($title).'"'.$attributes.'>'.\Image::getHtml($icon, $label, 'data-state="'.($row['TweetOnTwitter'] ? 1 : 0).'"').'</a> ';
        return '<a href="'.Controller::addToUrl($href).'&rt='.\RequestToken::get().'" title="'.\StringUtil::specialchars($title).'"'.$attributes.'>'.\Image::getHtml($icon, $label, 'data-state="'.($row['TweetOnTwitter'] ? 1 : 0).'"').'</a> ';
    }

    public function toggleTwitterNewsVisibility($intId, $blnVisible, \DataContainer $dc = null)
    {
        \Input::setGet('id', $intId);
        \Input::setGet('act', 'toggleTweeting');

        if ($dc) {
            $dc->id = $intId;
        }

        if (!\BackendUser::getInstance()->hasAccess(NewsModel::getTable().'::TweetOnTwitter', 'alexf')) {
            throw new \Contao\CoreBundle\Exception\AccessDeniedException('Not enough permissions to change twitter stats of News ID '.$intId.'.');
        }

        // Set the current record
        if ($dc) {
            $objRow = \Database::getInstance()->prepare('SELECT * FROM '.NewsModel::getTable().' WHERE id=?')
                ->limit(1)
                ->execute($intId);

            if ($objRow->numRows) {
                $dc->activeRecord = $objRow;
            }
        }

        $objVersions = new \Versions(NewsModel::getTable(), $intId);
        $objVersions->initialize();

        // Trigger the save_callback
        if (\is_array($GLOBALS['TL_DCA'][NewsModel::getTable()]['fields']['TweetOnTwitter']['save_callback'])) {
            foreach ($GLOBALS['TL_DCA'][NewsModel::getTable()]['fields']['TweetOnTwitter']['save_callback'] as $callback) {
                if (\is_array($callback)) {
                    \System::importStatic($callback[0]);
                    $blnVisible = \System::importStatic($callback[0])->{$callback[1]}($blnVisible, $dc);
                } elseif (\is_callable($callback)) {
                    $blnVisible = $callback($blnVisible, $dc);
                }
            }
        }

        $time = time();

        //run twitter action
        $this->toogleTwitterStatuses($intId, $blnVisible);

        // Update the database
        \Database::getInstance()->prepare('UPDATE '.NewsModel::getTable()." SET tstamp=$time, TweetOnTwitter='".($blnVisible ? '1' : '')."' WHERE id=?")
            ->execute($intId);

        if ($dc) {
            $dc->activeRecord->tstamp = $time;
            $dc->activeRecord->TweetOnTwitter = ($blnVisible ? '1' : '');
        }

        // Trigger the onsubmit_callback
        if (\is_array($GLOBALS['TL_DCA'][NewsModel::getTable()]['config']['onsubmit_callback'])) {
            foreach ($GLOBALS['TL_DCA'][NewsModel::getTable()]['config']['onsubmit_callback'] as $callback) {
                if (\is_array($callback)) {
                    \System::importStatic($callback[0])->{$callback[1]}($dc);
                } elseif (\is_callable($callback)) {
                    $callback($dc);
                }
            }
        }

        $objVersions->create();
    }

    // send news to twitter after change the tweet status
    public function toogleTwitterStatuses($intId, $status)
    {
        if ($status) {
            NewsTweets::tweetByNewsID($intId);
            \System::log("ID: $intId was create on twitter successfully", __METHOD__, TL_GENERAL);
        } else {
            NewsTweets::removeTweetByNewsID($intId);
            \System::log("ID: $intId was removed from twitter successfully", __METHOD__, TL_GENERAL);
        }
    }

    // send news to twitter after create or change a news
    public function SendThisNewsAfterSubmit(\DataContainer $dc)
    {
        // Return if there is no active record (override all)
        if (!$dc->activeRecord) {
            return;
        }

        if ($dc->activeRecord->TweetOnTwitter) {
            NewsTweets::tweetByNewsID($dc->id);
            \System::log("ID: $dc->id was create on twitter successfully", __METHOD__, TL_GENERAL);
        } else {
            NewsTweets::removeTweetByNewsID($dc->id);
            \System::log("ID: $dc->id was removed from twitter successfully", __METHOD__, TL_GENERAL);
        }
    }
}
