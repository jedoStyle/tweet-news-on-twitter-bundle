<?php

/*
 * This file is part of NewsTweetsBundle.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * This project is provided in good faith and hope to be usable by anyone.
 *
 * @package     Contao4 NewsTweetsBundle
 * @author      Jens Doberenz <WurzelGnOOm>
 * @copyright   jedoLabs 2019 <https://jedo-labs.de>
 * @license     LGPL-3.0-or-later
 * @see	        <https://gitlab.com/jedoLabs/>
 */

declare(strict_types=1);

namespace JedoLabs\ContaoNewstweetsBundle\Library;

use Abraham\TwitterOAuth\TwitterOAuth;

class TwitterAccess
{
    private $consumerKey;
    private $consumerSecret;
    private $accessKey;
    private $accessSecret;
    private $twitter;

    public function __construct($consumerKey, $consumerSecret, $accessKey, $accessSecret)
    {
        $this->consumerKey = $consumerKey;
        $this->consumerSecret = $consumerSecret;
        $this->accessKey = $accessKey;
        $this->accessSecret = $accessSecret;

        $this->twitter = new TwitterOAuth($consumerKey, $consumerSecret, $accessKey, $accessSecret);
    }

    public function sendPost($message, $picture)
    {
        if ($picture) {
            $UploadMedia = $this->twitter->upload('media/upload', ['media' => $picture]);
            $parameters = [
                'status' => $message,
                'media_ids' => $UploadMedia->media_id_string,
            ];
        } else {
            $parameters = [
                'status' => $message,
            ];
        }

        $response = $this->twitter->post('statuses/update', $parameters);

        if (\strlen($response->text) > 0) {
            return true;
        }

        return false;
    }

    public function findLastTweetforID($account)
    {
        $twitterAccount = '@'.$account;

        $lastTweet = $this->twitter->get('statuses/user_timeline', ['screen_name' => $twitterAccount, 'count' => 1]);

        // And now $tweetid is the variable that'll show your latest tweet ID
        $tweetid = $lastTweet[0]->id;

        if ($tweetid) {
            return $tweetid;
        }
    }

    public function searchTweet($message)
    {
        $tweets = $this->twitter->get('search/tweets', ['q' => $message, 'result_type' => 'recent']);

        if (\strlen($tweets->statuses) > 0) {
            return $tweets;
        }
    }

    public function removeTweet($TweetId)
    {
        //check to exsist the tweet
        $lookup = $this->twitter->get('statuses/lookup', ['id' => $TweetId]);

        $tweet = $lookup[0]->text;

        if (!$tweet) {

            return true;

        } else {

            $response = $this->twitter->post('statuses/destroy', ['id' => $TweetId]);

            if ('' === \strlen($response->text)) {
                return true;
            }
        }

        return false;
    }

    // check if Account is exsits on twitter an return the user_id << COMING SOON >>
    public function find_users($account)
    {
        $tAccount = $this->twitter->get('users/show', ['screen_name' => $account]);

        if ($tAccount->id) {
            return $tAccount;
        }
    }
}
